#include "Matrix.h"
#include <stdexcept>
#include "Mark.h"

template<typename T>
Matrix<T>::Matrix()
{

}

template<typename T>
Matrix<T>::Matrix(size_t rows, size_t cols)
{
    resize(rows, cols);
}

template<typename T>
Matrix<T>::Matrix(const std::vector<std::vector<T> > &mat) : m_matrix(mat)
{

}

template<typename T>
size_t Matrix<T>::rows() const
{
    return m_matrix.size();
}

template<typename T>
size_t Matrix<T>::cols() const
{
    if (m_matrix.empty())
    {
        return 0;
    }
    else
    {
        return m_matrix.at(0).size();
    }
}

template<typename T>
void Matrix<T>::resize(size_t rows, size_t cols)
{
    if (rows == 0)
    {
        m_matrix.clear();
        return;
    }

    for(size_t i = 0; i < m_matrix.size(); ++i)
    {
        m_matrix.at(i).resize(cols, 0);
    }

    std::vector<T> defaultRow(cols, 0);
    m_matrix.resize(rows, defaultRow);
}

template<>
void Matrix<Mark>::resize(size_t rows, size_t cols)
{
    if (rows == 0)
    {
        m_matrix.clear();
        return;
    }

    for(size_t i = 0; i < m_matrix.size(); ++i)
    {
        m_matrix.at(i).resize(cols, normal);
    }

    std::vector<Mark> defaultRow(cols, normal);
    m_matrix.resize(rows, defaultRow);
}

template<typename T>
void Matrix<T>::clear()
{
    m_matrix.clear();
}

template<typename T>
const T &Matrix<T>::operator()(size_t x, size_t y) const
{
    if (x < 0 || y < 0)
    {
        throw std::out_of_range("Index smaller than 0 when trying to access matrix element");
    }

    if (m_matrix.empty() || m_matrix.at(0).empty())
    {
        throw std::invalid_argument("Trying to access an empty matrix");
    }

    if (x >= m_matrix.size() || y >= m_matrix.front().size())
    {
        throw std::invalid_argument("Index out of range when trying to access matrix element");
    }

    return m_matrix.at(x).at(y);
}

template<typename T>
T &Matrix<T>::operator ()(size_t x, size_t y)
{
    if (x < 0 || y < 0)
    {
        throw std::out_of_range("Index smaller than 0 when trying to access matrix element");
    }

    if (m_matrix.empty() || m_matrix.at(0).empty())
    {
        throw std::invalid_argument("Trying to access an empty matrix");
    }

    if (x >= m_matrix.size() || y >= m_matrix.front().size())
    {
        throw std::invalid_argument("Index out of range when trying to access matrix element");
    }

    return m_matrix[x][y];
}

template class Matrix<float>;
template class Matrix<Mark>;
