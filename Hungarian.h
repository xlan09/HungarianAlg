/*
    This implementation is modified based on
    http://csclab.murraystate.edu/bob.pilgrim/445/munkres.html

    A good tutorial about Hungarian algorithm is referenced by the author:
    https://www.topcoder.com/community/data-science/data-science-tutorials/assignment-problem-and-hungarian-algorithm/
 */

#ifndef HUNGARIAN_H
#define HUNGARIAN_H

#include "Matrix.h"
#include "Mark.h"

class Hungarian
{
public:
    Hungarian(const Matrix<float> &mat);
    void solve();

private:
    Matrix<float> m_cost;
    Matrix<Mark> m_mask;
    std::vector<bool> m_rowCover;
    std::vector<bool> m_colCover;

    void resetMaskandCovers();
    int stepOne();
    int stepTwo();
    int stepThree();
    int stepFour(size_t &pathRowStart, size_t &pathColStart);
    int stepFive(size_t pathRowStart, size_t pathColStart);
    int stepSix();
    void stepSeven() const;

    bool findNoncoveredZero(size_t &row, size_t &col) const;
    bool findStarInRow(size_t row, size_t &col) const;
    bool findStarInCol(size_t &row, size_t col) const;
    bool findPrimeInRow(size_t row, size_t &col) const;
    float findSmallest() const;
    void showCostMatrix(int step) const;
    void showMaskMatrix(int step) const;
    void augmentPath(const std::vector<std::pair<size_t, size_t> > &path);
    void clearCovers();
    void erasePrimes();
};

#endif // HUNGARIAN_H
