#ifndef MATRIX_H
#define MATRIX_H
#include <vector>
#include <stddef.h>
#include <ostream>

template<typename T>
class Matrix
{
public:
    Matrix();
    Matrix(size_t rows, size_t cols);
    Matrix(const std::vector<std::vector<T> > &mat);

    size_t rows() const;
    size_t cols() const;
    void resize(size_t rows, size_t cols);
    void clear();
    const T& operator()(size_t x, size_t y) const;
    T &operator ()(size_t x, size_t y);

    friend std::ostream & operator<<(std::ostream &os, const Matrix<T> &matrix)
    {
        os << "Matrix:" << std::endl;
        for (size_t row = 0 ; row < matrix.rows() ; row++ )
        {
            for (size_t col = 0 ; col < matrix.cols() ; col++ )
            {
                os.width(8);
                os << matrix(row, col) << ",";
            }
            os << std::endl;
        }

        return os;
    }

private:
    std::vector<std::vector<T> > m_matrix;
};

#endif // MATRIX_H
