#ifndef UTIL_H
#define UTIL_H
#include<vector>
#include <stddef.h>


class Util
{
public:
    Util();
    static std::vector< std::vector<float> > array2Matrix(const float *m, size_t rows, size_t cols);
};

#endif // UTIL_H
