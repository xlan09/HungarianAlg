#include "Util.h"

Util::Util()
{

}

std::vector<std::vector<float> > Util::array2Matrix(const float *m, size_t rows, size_t cols)
{
    std::vector< std::vector<float> > res;
    res.resize(rows, std::vector<float>(cols, 0));

    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            res[i][j] = m[i * cols + j];
        }
    }

    return res;
}


