#include <iostream>
#include "Matrix.h"
#include "Util.h"
#include "Hungarian.h"

int main()
{
    /* an example cost matrix */
    float array[4*4] =  { 10, 20, 3, 40,
                          20, 48, 60, 8,
                          30, 6, 90, 120,
                          4, 800, 120, 160};
    std::vector< std::vector<float> > vec = Util::array2Matrix(array, 4, 4);
    Matrix<float> mat(vec);
    Hungarian solver(mat);
    try
    {
    solver.solve();
    }
    catch(const std::exception &exp)
    {
        std::cout << "Exceptions thrown: " << exp.what() << std::endl;
    }
}

