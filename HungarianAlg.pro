TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Matrix.cpp \
    Hungarian.cpp \
    Util.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    Matrix.h \
    Hungarian.h \
    Mark.h \
    Util.h

