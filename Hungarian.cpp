#include "Hungarian.h"
#include <limits>
#include <math.h>
#include <stdlib.h>
#include <stdexcept>
#include <iostream>

Hungarian::Hungarian(const Matrix<float> &mat) : m_cost(mat)
{
    size_t numRows = mat.rows(), numCols = mat.cols();
    m_mask.resize(numRows, numCols);
    m_rowCover.resize(numRows, false);
    m_colCover.resize(numCols, false);
}

void Hungarian::solve()
{
    bool done = false;
    int nextStep = 1;
    size_t pathRowStart = 0, pathColStart = 0;
    while (!done)
    {
        showCostMatrix(nextStep);
        showMaskMatrix(nextStep);
        switch (nextStep)
        {
        case 1:
            nextStep = stepOne();
            break;
        case 2:
            nextStep = stepTwo();
            break;
        case 3:
            nextStep = stepThree();
            break;
        case 4:
            nextStep = stepFour(pathRowStart, pathColStart);
            break;
        case 5:
            nextStep = stepFive(pathRowStart ,pathColStart);
            break;
        case 6:
            nextStep = stepSix();
            break;
        case 7:
            stepSeven();
            done = true;
            showMaskMatrix(7);
            break;
        }
    }
}

void Hungarian::resetMaskandCovers()
{
    for(size_t i = 0; i < m_mask.rows(); ++i)
    {
        for(size_t j = 0; j < m_mask.cols(); ++j)
        {
            m_mask(i, j) = normal;
        }
    }

    size_t numRows = m_rowCover.size(), numCols = m_colCover.size();
    m_rowCover.clear();
    m_rowCover.resize(numRows, false);
    m_colCover.clear();
    m_colCover.resize(numCols, false);
}

/**
 * @brief Hungarian::stepOne
 * For each row of the cost matrix, find the smallest element and subtract it
 * from every element in its row. Do the same for the columns, when finished,
 * Go to Step 2.
 * @return
 */
int Hungarian::stepOne()
{
    // subtract min element in each row
    for (size_t i = 0; i < m_cost.rows(); ++i)
    {
        float minInRow = m_cost(i, 0);
        for(size_t j = 0; j < m_cost.cols(); ++j)
        {
            minInRow = std::min(minInRow, m_cost(i, j));
        }

        for(size_t j = 0; j < m_cost.cols(); ++j)
        {
            m_cost(i, j) = m_cost(i, j) - minInRow;
        }
    }

    // subtract min element in each column
    for (size_t j = 0; j < m_cost.cols(); ++j)
    {
        float minInColumn = m_cost(0, j);
        for(size_t i = 0; i < m_cost.rows(); ++i)
        {
            minInColumn = std::min(minInColumn, m_cost(i, j));
        }

        for(size_t i = 0; i < m_cost.rows(); ++i)
        {
            m_cost(i, j) = m_cost(i, j) - minInColumn;
        }
    }

    int nextStep = 2;
    return nextStep;
}

/**
 * @brief Hungarian::stepTwo
 * Find a zero (Z) in the resulting matrix.  If the row and the column(which the
 * zero entry in) are not covered, star Z. Repeat for each element in the
 * matrix. Go to Step 3.
 * @return
 */
int Hungarian::stepTwo()
{
    for (size_t i = 0; i < m_cost.rows(); ++i)
    {
        for (size_t j = 0; j < m_cost.cols(); ++j)
        {
            if (m_cost(i, j) == 0 && m_rowCover[i] == false && m_colCover[j] == false)
            {
                m_mask(i, j) = star;
                m_rowCover[i] = true;
                m_colCover[j] = true;
            }
        }
    }

    // reset cover flag
    m_rowCover.clear();
    m_rowCover.resize(m_cost.rows(), false);
    m_colCover.clear();
    m_colCover.resize(m_cost.cols(), false);

    int nextStep = 3;
    return nextStep;
}

/**
 * @brief Hungarian::stepThree
 * Cover each column containing a starred zero.  If K columns are covered,
 * the starred zeros describe a complete set of unique assignments.  In this
 * case, Go to DONE, otherwise, Go to Step 4.
 * @return
 */
int Hungarian::stepThree()
{
    size_t coveredColCount = 0;
    for (size_t i = 0; i < m_cost.rows(); ++i)
    {
        for (size_t j = 0; j < m_cost.cols(); ++j)
        {
            if (m_mask(i, j) == star)
            {
                m_colCover[j] = true;
                coveredColCount++;
            }
        }
    }

    int nextStep = 0;
    if (coveredColCount >= m_cost.cols())
    {
        nextStep = 7;
    }
    else
    {
        nextStep = 4;
    }

    return nextStep;
}

/**
 * @brief Hungarian::stepFour
 * Find a noncovered zero and prime it.  If there is no starred zero
 * in the row containing this primed zero, Go to Step 5.  Otherwise,
 * cover this row and uncover the column containing the starred zero.
 * Continue in this manner until there are no uncovered zeros left.
 * Save the smallest uncovered value and Go to Step 6.
 * @return
 */
int Hungarian::stepFour(size_t &pathRowStart, size_t &pathColStart)
{
    size_t row = 0, col = 0;
    bool done = false;
    int nextStep = 0;

    while (!done)
    {
        if (!findNoncoveredZero(row, col))
        {
            done = true;
            nextStep = 6;
        }
        else
        {
            m_mask(row, col) = prime;
            if (findStarInRow(row, col))
            {
                m_rowCover[row] = true;
                m_colCover[col] = false;
            }
            else
            {
                done = true;
                nextStep = 5;
                pathRowStart = row;
                pathColStart = col;
            }
        }
    }

    return nextStep;
}

/**
 * @brief Hungarian::stepFive
 * Construct a series of alternating primed and starred zeros as follows.
        Let Z0 represent the uncovered primed zero found in Step 4.  Let Z1 denote
        the starred zero in the column of Z0 (if any). Let Z2 denote the primed zero
        in the row of Z1 (there will always be one).  Continue until the series
        terminates at a primed zero that has no starred zero in its column.
        Unstar each starred zero of the series, star each primed zero of the series,
        erase all primes and uncover every line in the matrix.  Return to Step 3.
 * @return
 */
int Hungarian::stepFive(size_t pathRowStart, size_t pathColStart)
{
    bool done = false;
    size_t r = 0, c = 0;
    std::vector<std::pair<size_t, size_t> > path;
    path.push_back(std::make_pair(pathRowStart, pathColStart));

    while (!done)
    {
        if (findStarInCol(r, path.back().second))
        {
            path.push_back(std::make_pair(r, path.back().second));
        }
        else
        {
            done = true;
        }

        if (!done)
        {
            findPrimeInRow(path.back().first, c);
            path.push_back(std::make_pair(path.back().first, c));
        }
    }

    augmentPath(path);
    clearCovers();
    erasePrimes();

    int nextStep = 3;
    return nextStep;
}

/**
 * @brief Hungarian::stepSix
 * Add the value found in Step 4 to every element of each covered row, and
 * subtract it from every element of each uncovered column.  Return to Step 4
 * without altering any stars, primes, or covered lines.
 * @return
 */
int Hungarian::stepSix()
{
    float minVal = findSmallest();
    if (abs(minVal - std::numeric_limits<float>::max()) < pow(10, -8))
    {
        throw std::logic_error("no smallest value in the cost matrix");
    }

    for (size_t r = 0; r < m_cost.rows(); ++r)
    {
        for (size_t c = 0; c < m_cost.cols(); ++c)
        {
            if (m_rowCover[r] == true)
            {
                m_cost(r, c) += minVal;
            }

            if (m_colCover[c] == false)
            {
                m_cost(r, c) -= minVal;
            }
        }
    }

    int nextStep = 4;
    return nextStep;
}

void Hungarian::stepSeven() const
{
    std::cout << "\n --------------Run Complete-----------" << std::endl;
}

bool Hungarian::findNoncoveredZero(size_t &row, size_t &col) const
{
    row = 0;
    col = 0;
    bool isFound = false;

    for(size_t i = 0; i < m_cost.rows(); ++i)
    {
        for(size_t j = 0; j < m_cost.cols(); ++j)
        {
            if (m_cost(i, j) == 0 && m_rowCover[i] == false && m_rowCover[j] == false)
            {
                row = i;
                col = j;
                isFound = true;
                break;
            }
        }

        if (isFound)
        {
            break;
        }
    }

    return isFound;
}

bool Hungarian::findStarInRow(size_t row, size_t &col) const
{
    col = 0;
    bool isFound = false;
    for (size_t j = 0; j < m_cost.cols(); ++j)
    {
        if (m_mask(row, j) == star)
        {
            col = j;
            isFound = true;
            break;
        }
    }

    return isFound;
}

bool Hungarian::findStarInCol(size_t &row, size_t col) const
{
    row = 0;
    bool isFound = false;
    for (size_t i = 0; i < m_cost.rows(); i++)
    {
        if (m_mask(i, col) == star)
        {
            row = i;
            isFound = true;
            break;
        }
    }

    return isFound;
}

bool Hungarian::findPrimeInRow(size_t row, size_t &col) const
{
    bool isFound = false;
    for (size_t j = 0; j < m_cost.cols(); ++j)
    {
        if (m_mask(row, j) == 2)
        {
            col = j;
            isFound = true;
            break;
        }
    }

    return isFound;
}

float Hungarian::findSmallest() const
{
    float minVal = std::numeric_limits<float>::max();
    for (size_t r = 0; r < m_cost.rows(); ++r)
    {
        for (size_t c = 0; c < m_cost.cols(); ++c)
        {
            if (m_rowCover[r] == false && m_colCover[c] == false && minVal > m_cost(r, c))
            {
                minVal = m_cost(r, c);
            }
        }
    }

    return minVal;
}

void Hungarian::showCostMatrix(int step) const
{
    std::cout<< "\n------------Step " << step << " Cost Matrix------------" << std::endl;
    for (size_t r = 0; r < m_cost.rows(); ++r)
    {
        std::cout << std::endl;
        for (size_t c = 0; c < m_cost.cols(); ++c)
        {
            std::cout << m_cost(r, c) << " ";
        }
    }
}

void Hungarian::showMaskMatrix(int step) const
{
    std::cout<< "\n------------Step " << step << " Mask Matrix------------" << std::endl;
    for (size_t r = 0; r < m_mask.rows(); ++r)
    {
        std::cout << std::endl;
        for (size_t c = 0; c < m_mask.cols(); ++c)
        {
            std::cout << m_mask(r, c) << " ";
        }
    }
}

void Hungarian::augmentPath(const std::vector<std::pair<size_t, size_t> > &path)
{
    for (size_t p = 0; p < path.size(); ++p)
    {
        if (m_mask(path[p].first, path[p].second) == star)
        {
            m_mask(path[p].first, path[p].second) = normal;
        }
        else
        {
            m_mask(path[p].first, path[p].second) = star;
        }
    }
}

void Hungarian::clearCovers()
{
    for (size_t r = 0; r < m_cost.rows(); ++r)
    {
        m_rowCover[r] = false;
    }

    for (size_t c = 0; c < m_cost.cols(); ++c)
    {
        m_colCover[c] = false;
    }
}

void Hungarian::erasePrimes()
{
    for (size_t r = 0; r < m_cost.rows(); ++r)
    {
        for (size_t c = 0; c < m_cost.cols(); ++c)
        {
            if (m_mask(r, c) == prime)
            {
                m_mask(r, c) = normal;
            }
        }
    }
}
